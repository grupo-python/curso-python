import sqlite3

def crear_base_datos(nombre):
    """
    docstring
    """
    nombre +=".db"
    conexion = sqlite3.connect(nombre)
    conexion.close()

def crear_tablas_bdd(nombre_tabla):
    """
    docstring
    """
    
    try:
        conexion = sqlite3.connect('ejemplo.db')

        # Creamos el cursor
        cursor = conexion.cursor()

        # Ahora crearemos una tabla de usuarios para almacenar nombres, edades y emails
        cursor.execute(f"CREATE TABLE {nombre_tabla} (nombre VARCHAR(100), edad INTEGER, email VARCHAR(100))")

        # Guardamos los cambios haciendo un commit
        conexion.commit()

        conexion.close()
    except Exception as ex:
        print("Error ",ex.)
    

def insertar_registro():
    """
    docstring
    """

    conexion = sqlite3.connect('ejemplo.db')
    cursor = conexion.cursor()

    # Insertamos un registro en la tabla de usuarios
    cursor.execute("INSERT INTO usuarios VALUES ('Hector', 27, 'hector@ejemplo.com')")

    # Guardamos los cambios haciendo un commit
    conexion.commit()

    conexion.close()


if __name__ == "__main__":
    crear_base_datos("ejemplo")
    crear_tablas_bdd("usuarios")
    insertar_registro()